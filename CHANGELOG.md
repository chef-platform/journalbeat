Changelog
=========

1.1.0
-----

Main:

- feat: set default journalbeat version to v5.6.9
- feat: handle root cas from data bag
- fix(install): typo in home directory resource
- feat: add var dir attribute
  + set data and logs in var dir by default

Tests:

- test: include .gitlab-ci.yml from test-cookbook
- test: replace deprecated require\_chef\_omnibus

Misc:

- feat: replace hard paths by symlinks in config
  + path in config file will use symbolic dirs (without version)
- chore: add 2018 to copyright notice
- doc: use doc in git message instead of docs

1.0.0
-----

- Initial version with logstash support, tested on Centos
